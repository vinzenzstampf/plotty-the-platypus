# Download file from repository
```bash
curl https://gitlab.com/dioguerra/plotty-the-platypus.git
cd plotty-the-platypus
```

# Step 1 - Move all csv files into the 'data' folder
Dont forget to rename the Column headers for the line values which
corresponde to the naming presented in the legend of the generated graph

# Step 2 - Create new configuration file for all csv files in data folder
This file must have the same name but end with '.json' instead of '.csv'
Parameters are as follows:

```json
{
  "window": [-0.5, 3], // [min_x, max_x] to print data
  "measure": true, // option to draw 'measure' lines [true/false]
  // values to search the first occurent in time or signal crosses the y_min and y_max values
  "search": [ 
    [-1000, -3000], // Line 1
    [-1000, -3000] // Line 2
        // Line n
  ]
}
```

# Step 3 - Install python and all dependencies
First check if it already exists by running in a console the command
```bash
python --version
python3 --version
```
If it does not, install
```bash

https://www.python.org/downloads/
```

Install dependencies
```bash
pip install -r requirements.txt
```

# Step 4 - Release the ~~CRACKEN!!!~~ charts!!!
Execute with
```bash
python src/plotty.py
```

# Step 5 - Check graphs in folder 'plots'


## TODO:
* Fix generator for different plots
