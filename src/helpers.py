import json
import os
import sys

import numpy as np

def get_index_of_closest_value(array, value):
    """
    Returns the index of the closest value in the array that first crosses the specified y value.

    Parameters:
    array (numpy.ndarray): The array to search.
    value (float): The y value to find the closest crossing point.

    Returns:
    int: The index of the closest value in the array that first crosses the specified y value.
    """
    # Ensure the array is a numpy array
    array = np.asarray(array, dtype=np.float64)
    
    # Find the indices where the array crosses the specified value
    cross_indices = np.where(np.diff(np.sign(array - value)) != 0)[0]

    if len(cross_indices) == 0:
        # If there are no crossing points, return the index of the closest value in the entire array
        closest_index = np.argmin(np.abs(array - value))
        return closest_index
    
    # Find the first crossing index
    first_cross_index = cross_indices[0]
    
    # Determine the closest value around the first crossing point
    closest_index = np.argmin(np.abs(array[first_cross_index:first_cross_index+2] - value)) + first_cross_index
    
    return closest_index

def read_config_file(file_path):
    """
    Reads a configuration JSON file and outputs a dictionary with specific parameters.

    Parameters:
    file_path (str): The path to the JSON file to read.

    Returns:
    dict: A dictionary with keys 'window', 'measure', and 'search' based on the JSON file.
    """

    if not os.path.exists(file_path):
        print(f"Error: The configuration file '{file_path}' was not found.")
        sys.exit(1) 

    # Read the JSON file
    with open(file_path, 'r') as file:
        config_data = json.load(file)

    # Extract required parameters
    config_dict = {
        'window': config_data.get('window'),
        'measure': config_data.get('measure', False),
        'search': config_data.get('search', []),
        'second_axis': config_data.get('second_axis', [])
    }

    return config_dict
